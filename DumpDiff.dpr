program DumpDiff;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils,
  System.StrUtils;

var
  verbose: boolean; // Flag for fuller info
  doscan: boolean; // Flag for scanning but no writing

  AllParameters: boolean;
  Parameter_Config: String;

  InputError: boolean;
  // Flag in parsing - something wrong with the config or parameters

  FilesArray1: Array of String;
  NumberOfFiles1: byte;

  FilesArray2: Array of String;
  NumberOfFiles2: byte;

  // The array of c64 files extracted from the configuration file

  I: byte;
  Getter: string;

procedure GenerateMatchArray(FileArray: Array of String;
  var MatchArray: Array of boolean);

var
  // The array that keeps the aggregated results of the comparison

  S1Byte, S2Byte: byte;
  S1, S2: File of byte;

  I: Integer;
  Count: Integer;

begin

  // Create an array that contains the number of blocks
  // to mark if there they are equal


  // Ok, now start to mke the comparison

  for Count := 1 to NumberOfFiles1 - 1 do // Loop for all the files
  begin
    AssignFile(S1, FileArray[0]);
    Reset(S1);
    AssignFile(S2, FileArray[Count]);
    Reset(S2);

    if verbose then
      Writeln(Output, 'Comparing the files ' + FileArray[0] + ' and ' +
        FileArray[Count]);

    // Setup and loop the file

    I := 0; // Points to current byte inside the block

    repeat
      Read(S1, S1Byte);
      Read(S2, S2Byte);
      if S1Byte <> S2Byte then
      begin
        MatchArray[I] := False;
      end;

      I := I + 1;
    until (EOF(S1));
    // until ((I = ArraySize) OR (EOF(S1)));

    CloseFile(S1);
    CloseFile(S2);

  end;
end;

Procedure PrintArray(MatchArray: Array of boolean; ArraySize: Integer);
var
  I: Integer;

begin

  Writeln(Output, '');
  Writeln(Output, 'Array overview:');
  Writeln(Output, '===============');

  Writeln(Output, 'Array size: ' + IntToStr(ArraySize));

  for I := 0 to ArraySize - 1 do
    if MatchArray[I] = True then
      Writeln(Output, 'Byte at $' + IntToHex(I, 4) + ' is set to True')
    else
      Writeln(Output, 'Byte at $' + IntToHex(I, 4) + ' is set to FALSE')
end;

/// MAIN CORE ROUTINE!

Procedure MakeTheShit(FilesArray1: Array of String;
  FilesArray2: Array of String);

var
  MatchArray1: Array of boolean;
  MatchArray2: Array of boolean;
  S1Byte, S2Byte: byte;
  S1, S2: File of byte;

  ArraySize: Integer;
  I: Integer;

begin
  ArraySize := 65536;

  // Generate the match array from the First files

  SetLength(MatchArray1, ArraySize);
  for I := 0 to ArraySize - 1 do
    MatchArray1[I] := True; // Frue = Equal, False = Different
  GenerateMatchArray(FilesArray1, MatchArray1);

  // Generate the match array from the other files

  SetLength(MatchArray2, ArraySize);
  for I := 0 to ArraySize - 1 do
    MatchArray2[I] := True; // Frue = Equal, False = Different
  GenerateMatchArray(FilesArray2, MatchArray1);

  // Print the stats of the compare array
  if doscan then
  begin

    PrintArray(MatchArray1, ArraySize);
    PrintArray(MatchArray2, ArraySize);
  end;

  AssignFile(S1, FilesArray1[0]);
  Reset(S1);
  AssignFile(S2, FilesArray2[0]);
  Reset(S2);

  for I := 0 to ArraySize - 1 do
  begin
    Read(S1, S1Byte);
    Read(S2, S2Byte);

    if ((MatchArray1[I] = True) AND (MatchArray2[I] = True)) then
    begin
      if S1Byte <> S2Byte then
        Writeln(Output, 'Difference at $' + IntToHex(I, 4) + '. Before: $' +
          IntToHex(S1Byte, 2) + '  After: $' + IntToHex(S2Byte, 2))

    end;

  end;

  CloseFile(S1);
  CloseFile(S2);

end;

Function CountFiles(ConfigFile: String): Integer;

var
  C1: textfile;

begin
  AssignFile(C1, ConfigFile);
  Reset(C1);

  // Count array entries

  I := 0;
  While NOT EOF(C1) do
  begin
    Readln(C1, Getter);
    // Just read dummy strings to establish number of files
    I := I + 1;
  end;
  Result := I;

  // Program assumes that there is two files or more
  if I < 2 then
  begin
    Writeln(Output, 'The program needs at least two files to work.');
    InputError := True;
  end;

  CloseFile(C1);
end;

Procedure PopulateArray(ConfigFile: String; var FilesArray: Array of String);

var
  C1: textfile;

begin
  AssignFile(C1, ConfigFile);
  Reset(C1);

  I := 0;
  While NOT EOF(C1) do
  begin
    Readln(C1, FilesArray[I]);

    if verbose then
      Writeln(Output, 'File to be processed: ' + FilesArray[I]);

    // Validate that the files in the config exists
    if NOT Fileexists(FilesArray[I]) then
    begin
      Writeln(Output, 'File in config file not found: ' + FilesArray[I]);
      InputError := True;
    end;
    I := I + 1;

  end;

  CloseFile(C1);
end;

procedure FindParameter(SearchString: String; Var ResultString: String);
var
  I: Integer;

begin

  // Writeln(Output, 'Search for parameter: ' + SearchString); // test

  ResultString := '';
  for I := 0 to ParamCount do
  begin
    if pos(SearchString, lowercase(ParamStr(I))) = 2 then
    begin
      ResultString := (ParamStr(I + 1));
      // Writeln(Output, 'Found the parameter: ' + ResultString); // test
      exit;
    end;
  end;

end;

// This is the main ...

begin
  AllParameters := False;
  InputError := False;

  try
    Writeln(Output, 'DumpDiff 1.0 beta 1 - (C) 2018 by Pontus "Bacchus" Berg');
    Writeln(Output, '');

    if (FindCmdLineSwitch('h', True) or FindCmdLineSwitch('?') or
      (ParamCount = 0)) then
    begin
      Writeln(Output,
        'Purpose: Identify real, non-volatile, changes between c64 memory dumps');
      Writeln(Output, 'Usage:   DumpDiff /conf1 /conf2 [/v] [/h] [/?] [/scan]');
      Writeln(Output, '');
      Writeln(Output,
        '/conf1       the file that contains the names of the files to be examined');
      Writeln(Output,
        '/conf2       the file that contains the names of the files to be examined');
      Writeln(Output, '/h or /?     help (this)');
      Writeln(Output, '/v           verbose output');
      Writeln(Output, '/scan        print of the delta array');
      exit;
    end
    else
    begin
      AllParameters := True;
      InputError := False;

      verbose := False;
      If FindCmdLineSwitch('v', True) then
      begin
        verbose := True;
      end;

      doscan := False;
      If FindCmdLineSwitch('scan', True) then
      begin
        doscan := True;
      end;

      If FindCmdLineSwitch('conf1', True) then
      begin
        FindParameter('conf1', Parameter_Config);
        if Fileexists(Parameter_Config) = False then
        begin
          AllParameters := False;
          Writeln('ERROR: Configuration file 1 doesn''t exist');
        end
        else
        begin
          if verbose then
            Writeln(Output, 'Config file1: ' + Parameter_Config);

          // Set the size of the array
          SetLength(FilesArray1, CountFiles(Parameter_Config) + 1);
          NumberOfFiles1 := CountFiles(Parameter_Config);
          PopulateArray(Parameter_Config, FilesArray1);

        end;
      end
      else
      begin
        // Option if the configuration file is not provided
        AllParameters := False;
        Writeln('ERROR: Configuration file 1 parameter not provided');
      end;

      If FindCmdLineSwitch('conf2', True) then
      begin
        FindParameter('conf2', Parameter_Config);
        if Fileexists(Parameter_Config) = False then
        begin
          AllParameters := False;
          Writeln('ERROR: Configuration file 2 doesn''t exist');
        end
        else
        begin
          if verbose then
            Writeln(Output, 'Config file2: ' + Parameter_Config);

          SetLength(FilesArray2, CountFiles(Parameter_Config) + 1);
          NumberOfFiles2 := CountFiles(Parameter_Config);
          PopulateArray(Parameter_Config, FilesArray2);

        end;
      end
      else
      begin
        // Option if the configuration file is not provided
        AllParameters := False;
        Writeln('ERROR: Configuration file 3 parameter not provided');
      end;

    end;

    // Here all parameters and prerequisites should be validated and the corresponding flags set

    if ((AllParameters = True) AND (InputError = False)) then
    begin
//      Writeln(Output, 'Execute the main program');
      MakeTheShit(FilesArray1, FilesArray2);
    end
    else
      Writeln(Output, 'Error in the input data - fix and run me again');

  except
    on E: Exception do
    begin
      Writeln(E.ClassName, ': ', E.Message);
      // Writeln(Output, 'Shit is bugging');
    end;

  end;

end.
